﻿using Heritage.Enums;

namespace Heritage.Models
{
    public class AuthResponse
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }

        public Role Role { get; set; }
    }
}
