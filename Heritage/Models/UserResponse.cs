﻿using Heritage.Enums;

namespace Heritage.Models
{
    public class UserResponse
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; }
    }
}
