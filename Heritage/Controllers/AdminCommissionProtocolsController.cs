﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Heritage.Data;
using Heritage.Data.Entities;
using Microsoft.AspNetCore.Authorization;

namespace Heritage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminCommissionProtocolsController : ControllerBase
    {
        private readonly HeritageDbContext _context;

        public AdminCommissionProtocolsController(HeritageDbContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "Admin,User")]
        // GET: api/AdminCommissionProtocols
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AdminCommissionProtocol>>> GetAdminCommissionProtocols()
        {
            return await _context.AdminCommissionProtocols.ToListAsync();
        }

        [Authorize(Roles = "Admin,User")]
        // GET: api/AdminCommissionProtocols/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AdminCommissionProtocol>> GetAdminCommissionProtocol(int id)
        {
            var adminCommissionProtocol = await _context.AdminCommissionProtocols.FindAsync(id);

            if (adminCommissionProtocol == null)
            {
                return NotFound();
            }

            return adminCommissionProtocol;
        }

        [Authorize(Roles = "Admin,User")]
        // PUT: api/AdminCommissionProtocols/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdminCommissionProtocol(int id, AdminCommissionProtocol adminCommissionProtocol)
        {
            if (id != adminCommissionProtocol.Id)
            {
                return BadRequest();
            }

            _context.Update(adminCommissionProtocol);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdminCommissionProtocolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize(Roles = "Admin,User")]
        // POST: api/AdminCommissionProtocols
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<AdminCommissionProtocol>> PostAdminCommissionProtocol(AdminCommissionProtocol adminCommissionProtocol)
        {
            _context.AdminCommissionProtocols.Add(adminCommissionProtocol);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAdminCommissionProtocol", new { id = adminCommissionProtocol.Id }, adminCommissionProtocol);
        }

        [Authorize(Roles = "Admin,User")]
        // DELETE: api/AdminCommissionProtocols/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAdminCommissionProtocol(int id)
        {
            var adminCommissionProtocol = await _context.AdminCommissionProtocols.FindAsync(id);
            if (adminCommissionProtocol == null)
            {
                return NotFound();
            }

            _context.AdminCommissionProtocols.Remove(adminCommissionProtocol);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AdminCommissionProtocolExists(int id)
        {
            return _context.AdminCommissionProtocols.Any(e => e.Id == id);
        }
    }
}
