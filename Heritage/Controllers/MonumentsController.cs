﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Heritage.Data;
using Heritage.Data.Entities;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.AspNetCore.Authorization;

namespace Heritage.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonumentsController : ControllerBase
    {
        private readonly HeritageDbContext _context;

        public MonumentsController(HeritageDbContext context)
        {
            _context = context;
        }

        // GET: api/Monuments
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Monument>>> GetMonuments()
        {
            return await _context.Monuments.ToListAsync();
        }

        // GET: api/Monuments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Monument>> GetMonument(int id)
        {
            var monument = await _context.Monuments.FindAsync(id);

            if (monument == null)
            {
                return NotFound();
            }

            return monument;
        }

        [HttpGet("files/{fileId}")]
        public async Task<IActionResult> DownloadFile(int fileId)
        {
            AttachedFile? attachedFile = await _context.AttachedFiles.FindAsync(fileId);
            if (attachedFile == null) return NotFound();

            return new FileContentResult(attachedFile.Bytes, "application/octet-stream")
            {
                FileDownloadName = attachedFile.Name,
            };
        }

        [Authorize(Roles = "Admin,User")]
        // PUT: api/Monuments/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMonument(int id, Monument monument)
        {
            if (id != monument.Id || monument.Address != null && id != monument.Address.Id)
            {
                return BadRequest();
            }

            _context.Update(monument);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MonumentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [Authorize(Roles = "Admin,User")]
        // POST: api/Monuments
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Monument>> PostMonument(Monument monument)
        {
            _context.Monuments.Add(monument);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMonument", new { id = monument.Id }, monument);
        }

        [Authorize(Roles = "Admin,User")]
        [HttpPost("{id}/files")]
        public async Task<IActionResult> UploadFile(int id, IFormFile file)
        {
            if (file == null || file.Length == 0)
                return BadRequest("No file uploaded.");

            Monument? monument = await _context.Monuments.FindAsync(id);
            if (monument == null) return NotFound();

            AttachedFile attachedFile = new() { Name = file.Name, Monument = monument };

            using MemoryStream memoryStream = new();
            await file.CopyToAsync(memoryStream);
            attachedFile.Bytes = memoryStream.ToArray();

            await _context.AttachedFiles.AddAsync(attachedFile);
            await _context.SaveChangesAsync();

            return Ok(attachedFile.Id);
        }

        [Authorize(Roles = "Admin,User")]
        // DELETE: api/Monuments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMonument(int id)
        {
            var monument = await _context.Monuments.FindAsync(id);
            if (monument == null)
            {
                return NotFound();
            }

            _context.Monuments.Remove(monument);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [Authorize(Roles = "Admin,User")]
        [HttpDelete("files/{fileId}")]
        public async Task<IActionResult> DeleteFile(int fileId)
        {
            AttachedFile? attachedFile = await _context.AttachedFiles.FindAsync(fileId);
            if (attachedFile == null)
            {
                return NotFound();
            }

            _context.AttachedFiles.Remove(attachedFile);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MonumentExists(int id)
        {
            return _context.Monuments.Any(e => e.Id == id);
        }
    }
}
