﻿using Heritage.Data.Entities;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;

namespace Heritage.Services
{
    public class TokenService(ILogger<TokenService> logger, IConfiguration configuration)
    {
        private const int _expirationMinutes = 120;
        private readonly string _privateKey = configuration.GetSection("JwtTokenSettings")["SymmetricSecurityKey"] ?? "SecretKey";

        public string GenerateToken(ApplicationUser user)
        {
            JsonWebTokenHandler handler = new();
            var key = Encoding.ASCII.GetBytes(_privateKey);
            var credentials = new SigningCredentials(
                new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature
            );

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = GenerateClaims(user),
                Expires = DateTime.UtcNow.AddMinutes(_expirationMinutes),
                SigningCredentials = credentials,
            };

            var token = handler.CreateToken(tokenDescriptor);
            logger.LogInformation("JWT Token created");

            return token;
        }

        private static ClaimsIdentity GenerateClaims(ApplicationUser user)
        {
            try
            {
                var claims = new ClaimsIdentity();

                //claims.AddClaim(new(ClaimTypes.Name, user.UserName));
                claims.AddClaim(new(ClaimTypes.Name, user.Email));
                claims.AddClaim(new(ClaimTypes.Role, user.Role.ToString()));

                return claims;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        //public string CreateToken(ApplicationUser user)
        //{
        //    var expiration = DateTime.UtcNow.AddMinutes(_expirationMinutes);
        //    var token = CreateJwtToken(
        //        CreateClaims(user),
        //        CreateSigningCredentials(),
        //        expiration
        //    );
        //    var tokenHandler = new JwtSecurityTokenHandler();

        //    logger.LogInformation("JWT Token created");

        //    return tokenHandler.WriteToken(token);
        //}

        //private JwtSecurityToken CreateJwtToken(List<Claim> claims, SigningCredentials credentials,
        //    DateTime expiration) =>
        //    new(
        //        new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("JwtTokenSettings")["ValidIssuer"],
        //        new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("JwtTokenSettings")["ValidAudience"],
        //        claims,
        //        expires: expiration,
        //        signingCredentials: credentials
        //    );

        //private List<Claim> CreateClaims(ApplicationUser user)
        //{
        //    var jwtSub = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("JwtTokenSettings")["JwtRegisteredClaimNamesSub"];

        //    try
        //    {
        //        var claims = new List<Claim>
        //        {
        //            new Claim(JwtRegisteredClaimNames.Sub, jwtSub),
        //            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        //            new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString()),
        //            new Claim(ClaimTypes.NameIdentifier, user.Id),
        //            new Claim(ClaimTypes.Name, user.UserName),
        //            new Claim(ClaimTypes.Email, user.Email),
        //            new Claim(ClaimTypes.Role, user.Role.ToString())
        //        };

        //        return claims;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}

        //private SigningCredentials CreateSigningCredentials()
        //{
        //    var symmetricSecurityKey = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("JwtTokenSettings")["SymmetricSecurityKey"];

        //    return new SigningCredentials(
        //        new SymmetricSecurityKey(
        //            Encoding.UTF8.GetBytes(symmetricSecurityKey)
        //        ),
        //        SecurityAlgorithms.HmacSha256
        //    );
        //}
    }
}
