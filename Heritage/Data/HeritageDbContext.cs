﻿using Heritage.Data.Entities;
using Heritage.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Heritage.Data
{
    public class HeritageDbContext(DbContextOptions<HeritageDbContext> options, IConfiguration configuration) : IdentityUserContext<ApplicationUser>(options)
    {
        readonly IConfiguration _configuration = configuration;

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Monument> Monuments { get; set; }

        public DbSet<AttachedFile> AttachedFiles { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<AdminCommissionProtocol> AdminCommissionProtocols { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseLazyLoadingProxies()
                .UseNpgsql(_configuration.GetConnectionString("PostgreSQL"));

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Seed AspNetUsers table with default admin user
            var hasher = new PasswordHasher<ApplicationUser>();

            var adminEmail = _configuration.GetSection("SiteSettings")["AdminEmail"];
            var adminPassword = _configuration.GetSection("SiteSettings")["AdminPassword"];

            builder.Entity<ApplicationUser>().HasData(
                new ApplicationUser
                {
                    Id = "80c8b6b1-e2b6-45e8-b044-8f2178a90111",
                    UserName = "admin",
                    NormalizedUserName = adminEmail.ToUpper(),
                    PasswordHash = hasher.HashPassword(null, adminPassword),
                    Email = adminEmail,
                    NormalizedEmail = adminEmail.ToUpper(),
                    Role = Role.Admin
                }
            );
        }
    }
}
