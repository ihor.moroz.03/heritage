﻿namespace Heritage.Data.Entities
{
    public class AdminCommissionProtocol
    {
        public int Id { get; set; }

        public DateOnly Date { get; set; }

        public string? SerialNumber { get; set; }

        public DateOnly? CompilationDate { get; set; }

        public int DecreeNumber { get; set; }

        public int CaseNumber { get; set; }

        public virtual Address Address { get; set; }

        public string Substance { get; set; }

        public string OffenderName { get; set; }

        public DateOnly? OffenderBirthDate { get; set; }

        public string OffenderPassportInfo { get; set; }

        public decimal FineAmount { get; set; }

        public string FineAmountString { get; set; }

        public decimal TaxFreeMinimums { get; set; }

        public decimal RepeatedFineAmount { get; set; }

        public string RepeatedFineAmountString { get; set; }

        public decimal RepeatedTaxFreeMinimums { get; set; }

        public string? RegisterNo { get; set; }

        public string? Note { get; set; }
    }
}
