﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Heritage.Data.Entities
{
    public class AttachedFile
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; } = null!;

        [JsonIgnore]
        public byte[] Bytes { get; set; } = [];

        public DateOnly UploadDate { get; private init; } = DateOnly.FromDateTime(DateTime.Now);

        [NotMapped]
        public string Size => BytesToString(Bytes?.LongLength ?? 0);

        [JsonIgnore]
        public virtual Monument Monument { get; set; } = null!;

        static string BytesToString(long byteCount)
        {
            string[] suf = ["B", "KB", "MB", "GB"];
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 2);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }
    }
}
