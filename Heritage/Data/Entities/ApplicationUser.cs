﻿using Heritage.Enums;
using Microsoft.AspNetCore.Identity;
using System.Reflection.Metadata.Ecma335;

namespace Heritage.Data.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public Role Role { get; set; }
    }
}
