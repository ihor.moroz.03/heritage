﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Heritage.Data.Entities
{
    public class Monument
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual MonumentAddress? Address { get; set; }

        public string Dates { get; set; }

        public string Type { get; set; }

        public string ProtectionDecision { get; set; }

        public string ProtectionNumber { get; set; }

        public string NationalRegisterDocs { get; set; }

        public string Category { get; set; }

        public string? GeneralCondition { get; set; }

        public BalconyConditions BalconyCondition { get; set; } = BalconyConditions.None;

        public virtual ICollection<AttachedFile> AttachedFiles { get; } = [];

        public enum BalconyConditions
        {
            None,
            Good,
            Satisfactory,
            UnSatisfactory,
            Wreck
        }
    }
}
