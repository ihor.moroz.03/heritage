﻿namespace Heritage.Data.Entities
{
    public class Order
    {
        public int Id { get; set; }

        public DateOnly Date { get; set; }

        public string? RegionAddressee { get; set; }

        public string Owner { get; set; }

        public string Addressee { get; set; }

        public string Substanse { get; set; }

        public virtual Address Address { get; set; }

        public string OrderNumber { get; set; }

        public string? Completion { get; set; }

        public string? Note { get; set; }
    }
}
