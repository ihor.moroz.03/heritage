﻿namespace Heritage.Data.Entities
{
    public class Address
    {
        public int Id { get; set; }

        public virtual MonumentAddress MonumentAddress { get; set; }

        public string? AddressLine2 { get; set; }
    }
}
