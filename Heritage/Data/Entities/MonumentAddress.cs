﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Heritage.Data.Entities
{
    public class MonumentAddress
    {
        [Key, ForeignKey("Monument")]
        public int Id { get; set; }

        public string AddressLine1 { get; set; }

        public string City { get; set; }

        public string Zip { get; set; }

        [JsonIgnore]
        public virtual Monument? Monument { get; set; }
    }
}
